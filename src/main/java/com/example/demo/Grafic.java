package com.example.demo;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.DefaultXYDataset;

import java.awt.*;

public class Grafic {
    String xAxisLabel="hour";
    static String yAxisLabel="measurement_value";
    static String title="Chart measurement_values/hour";
    static Boolean legend=true;
    static Boolean tooltips=true;
    static Boolean urls=false;

    public ChartPanel createChartPanel() {

        DefaultXYDataset ds = new DefaultXYDataset();

        double[][] data = { {}, {} };

        ds.addSeries("days", data);

        JFreeChart chart =
                ChartFactory.createXYLineChart(title,
                        xAxisLabel, yAxisLabel, ds, PlotOrientation.VERTICAL, legend, tooltips,
                        urls);
        chart.setBorderVisible(true);
        return new ChartPanel(chart);
    }
}
