package com.example.demo;

import java.util.Date;
import java.util.UUID;

public interface IObjectRemote {


    public String viewConnection(String connection);
    public double[][] dates(UUID id,Date date,  int days);
}
