package com.example.demo;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.DefaultXYDataset;
import org.springframework.boot.SpringApplication;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.UUID;

public class JDatePicker extends JFrame implements ActionListener {


    private JDatePickerImpl datePicker;
    private ChartPanel createChart;
    static JTextField tDays;
    static JLabel lDays;
    static JLabel lSensor;
    static JLabel lCalendar;
    static JTextField tSensor;
    private JButton buttonView;
    JPanel p = new JPanel();
    JPanel p2 = new JPanel();
    String xAxisLabel="hour";
    static String yAxisLabel="measurement_value";
    static String title="Chart measurement_values/hour";
    static Boolean legend=true;
    static Boolean tooltips=true;
    static Boolean urls=false;


    public JDatePicker() {
        super("Consumption Chart ");
        setLayout(new FlowLayout());

        add(new JLabel("Day: "));



        UtilDateModel model = new UtilDateModel();
        model.setDate(2021, 12, 24);
        model.setSelected(true);

        JDatePanelImpl datePanel = new JDatePanelImpl(model);

        datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

        Grafic grafic=new Grafic();
        createChart =grafic.createChartPanel();




        lDays=new JLabel("Enter the number of days:");
        lSensor=new JLabel("Enter the sensor id:");
        lCalendar=new JLabel("Pick a date:");
        p.add(lDays);
        p2.add(lSensor);
        tDays = new JTextField(7);
        tDays.setText("7");
        tSensor = new JTextField(20);
        tSensor.setText("5c2494a3-1140-4c7a-991a-a1a2561c6bc2");
        p.add(tDays);
        p2.add(tSensor);
        add(p);
        add(p2);



        add(lCalendar);
        add(datePicker);
        JButton buttonView = new JButton("View");
        buttonView.addActionListener(this);

        add(buttonView);


        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);


    }



    private  double[][] days_hours(Date input_date, int nrOfDays, UUID idSensor)
    {


        IObjectRemote service= SpringApplication.run(DemoApplication.class).getBean(IObjectRemote.class);

        double[][] measurement_values = new double[nrOfDays][24];
               measurement_values= service.dates(idSensor,input_date,nrOfDays);
        for (int day = 0; day < nrOfDays; day++) {
            for(int hour = 0; hour < 24; hour++) {
                System.out.println("values["+day+"]["+hour+"] = " + measurement_values[day][hour]);
            }
        }
        return measurement_values;

    }



    @Override
    public void actionPerformed(ActionEvent event) {

        String sensor_id=tSensor.getText();
        UUID idSensor = UUID.fromString(sensor_id);
        Date selectedDate = (Date) datePicker.getModel().getValue();
        System.out.println("Selected date is "+selectedDate);

        DefaultXYDataset dataSet = new DefaultXYDataset();
        JFreeChart chart =
                ChartFactory.createXYLineChart(title+" for sensor with id : "+ sensor_id,
                        xAxisLabel, yAxisLabel, dataSet, PlotOrientation.VERTICAL, legend, tooltips,
                        urls);

        chart.setBorderVisible(true);
        chart.setBackgroundPaint(Color.pink);
        chart.getBackgroundImageAlpha();
        chart.getBorderStroke();


        IObjectRemote service= SpringApplication.run(DemoApplication.class).getBean(IObjectRemote.class);

        JFrame frame = new JFrame("Charts");

        frame.setSize(600, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        System.out.println(service.viewConnection(""));



        //*****default
        //int nrOfDays = 7;
        String days=tDays.getText();
        int nrOfDays=Integer.parseInt(days);

        //double[][] values=days_hours(selectedDate,nrOfDays,idSensor);

         Date today=selectedDate;
        System.out.println(today);
        double[][] values = service.dates(idSensor,today,nrOfDays);
        for (int day = 0; day < nrOfDays; day++) {
            for(int hour = 0; hour < 24; hour++) {
                System.out.println("measurement_values["+day+"]["+hour+"] = " + values[day][hour]);
            }
        }

        double[] y=new double[24];
        int hour=0;
        while(hour<y.length)
        {
            y[hour]=hour;
            hour++;

        }



        int i=0;
         while(i<nrOfDays)
        {
            double[][] measurement_values = { y, values[i]};
            dataSet.addSeries("day"+(i+1),measurement_values);
            i++;
        }

        createChart.setChart(chart);

        frame.getContentPane().add(createChart);






    }

}