package com.example.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

import javax.swing.*;

@SpringBootApplication
public class DemoApplication {



	@Bean
	public HessianProxyFactoryBean hessianInvoker() {
		HessianProxyFactoryBean invoker = new HessianProxyFactoryBean();
		invoker.setServiceUrl("https://spring-demo-sd2021-andreea.herokuapp.com/measurement_export_value");
		invoker.setServiceInterface(IObjectRemote.class);
		return invoker;
	}
	public static void main(String [] arg) {

		System.setProperty("java.awt.headless", "false");
		//View v=new View();
		//v.initialGUI();
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new JDatePicker().setVisible(true);
			}
		});


	}


}